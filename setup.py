import sys
# Remove current dir from sys.path, otherwise setuptools will peek up our
# module instead of system's.
sys.path.pop(0)
from setuptools import setup
sys.path.append("..")
import sdist_upip

setup(name='micropython-unittest_junit',
      version='1.1',
      description='Extended unittest for MicroPython with junit xml output',
      long_description="Extended unittest for MicroPython with junit xml output and utility to run all unit tests in a file tree",
      url='https://gitlab.com/alelec/micropython_unittest_junit',
      author='Andrew Leech',
      author_email='andrew@alelec.net',
      license='MIT',
      cmdclass={'sdist': sdist_upip.sdist},
      packages=['unittest', 'xml.etree'],
      py_modules=['junit_xml'])
