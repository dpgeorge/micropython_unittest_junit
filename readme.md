# micropython-unittest_junit

This package provides a micropython unittest framework which can be used to run all unit tests in a project and output the results as a JUnit xml file for automated parsing and reporting.

It includes an extended version of https://github.com/micropython/micropython-lib/tree/master/unittest and a wrapper library to write the results files.

As this package calls itself `unittest` it can be used as a replacement for the standard `unittest` package and run any existing unit tests based on that.

It can also be run as a command line utility to run all unit tests from the current working directory file tree:
```
    $ cd /path/to/app/src
    $ micropython -m unittest

```